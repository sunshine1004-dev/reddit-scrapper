export default () => (
  <div className="flex items-center gap-4">
    <label
      htmlFor="theme-setting"
      className="dark:text-gray-300 text-lg font-bold"
    >
      Dark
    </label>
    <input
      type="checkbox"
      defaultChecked
      id="theme-setting"
      onChange={(e) => {
        if (e.target.checked) {
          document.documentElement.classList.add("dark");
        } else {
          document.documentElement.classList.remove("dark");
        }
      }}
    />
  </div>
);
