import { useState } from "react";
import { getComments } from "./api";
import Dark from "../../components/dark";

export default () => {
  const [keyword, setKeyword] = useState<string>("");
  const [youtubeVideos, setYoutubeVideos] = useState([]);
  const [youtubeLoading, setYoutubeLoading] = useState(false);

  const getData = async () => {
    setYoutubeLoading(true);
    setYoutubeVideos([]);
    const videos = await getComments(keyword);

    setYoutubeLoading(false);
    setYoutubeVideos(videos);
  };

  return (
    <div className="bg-gray-100 dark:bg-gray-800 min-h-screen">
      <div className="container mx-auto">
        <div className="w-full flex gap-4 items-center ml-2 pt-8 mb-8">
          <label
            htmlFor="search_keywords"
            className="text-xl font-bold dark:text-gray-300"
          >
            Search Keywords:
          </label>
          <input
            type="text"
            className="p-1.5"
            id="search_keywords"
            name="search_keywords"
            value={keyword}
            onChange={(e) => setKeyword(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                getData();
              }
            }}
          />
          <button onClick={getData}>Search</button>
          <Dark />
        </div>
        <div className="w-full">
          <div className="w-full flex sm:justify-end justify-center">
            <div className="w-full">
              {youtubeLoading && (
                <div className="w-full text-xl dark:text-gray-300">
                  Loading...
                </div>
              )}
              {youtubeVideos.map((comment: any, key) => (
                <div
                  key={key}
                  className="bg-white p-4 rounded shadow mb-2 w-full flex flex-row items-center justify-between gap-4"
                >
                  <div className="flex gap-4 items-center">
                    <a
                      href={`https://www.youtube.com/watch?v=${comment.snippet.videoId}`}
                      className="text-xl font-bold"
                      target="_blank"
                    >
                      Video
                    </a>
                    <div>
                      <p
                        className="text-gray-800"
                        dangerouslySetInnerHTML={{
                          __html:
                            comment.snippet.topLevelComment.snippet.textDisplay,
                        }}
                      ></p>
                      <a
                        className="text-gray-600"
                        target="_blank"
                        href="{{comment.snippet.topLevelComment.snippet.authorChannelUrl}}"
                      >
                        {
                          comment.snippet.topLevelComment.snippet
                            .authorDisplayName
                        }
                      </a>
                      <p className="text-gray-600">
                        {comment.snippet.topLevelComment.snippet.publishedAt}
                      </p>
                    </div>
                  </div>
                  <a
                    href="https://www.youtube.com/watch?v={{video.id.videoId}}&lc={{comment.id}}"
                    target="_blank"
                    className="text-xl font-bold"
                  >
                    Reply
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
