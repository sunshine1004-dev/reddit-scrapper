import axios from "axios";
const API = import.meta.env.VITE_APP_API || 'http://localhost:5000'
export const getComments = async (search: string) => {
  const res = await axios.get(
    `${API}/search-youtube-comments-api/?search_keywords=${search}`
  );
  return res.data.result
};
