import * as Dialog from "@radix-ui/react-dialog";
import * as Tabs from "@radix-ui/react-tabs";
import { SettingIcon } from "../../components/icons/setting-icon";
import { subreddits_queries } from "../../config";
import { useState, useRef } from "react";

type Props = {
  fetchReddits: (queries: string[]) => void;
  exportJSON: () => void;
  importJSON: (data: string[]) => void;
};

export const SubredditSettingModal = (props: Props) => {
  const [queries, setQueries] = useState<string[]>([]);
  const [activeTab, setActiveTab] = useState<"keyword" | "json">("keyword");

  const closeRef = useRef<HTMLButtonElement>(null);
  const hiddenFileInput = useRef<HTMLInputElement>(null);

  const handleFileRead = (file: any) => {
    const reader = new FileReader();

    reader.onload = function (event) {
      const jsonObj = JSON.parse(event.target?.result as string);
      props.importJSON(jsonObj);
      closeRef.current?.click();
    };

    reader.readAsText(file);
  };

  const handleFileUpload = (event: any) => {
    const file = event.target.files[0];
    if (file) {
      handleFileRead(file);
    }
  };

  const handleCheck = (query: string) => {
    if (queries.includes(query)) {
      setQueries(queries.filter((item) => item !== query));
    } else {
      setQueries(queries.concat(query));
    }
  };

  const handleSave = () => {
    if (activeTab === "keyword") {
      props.fetchReddits(queries);
    }
  };

  const handleExport = () => {
    props.exportJSON();
    closeRef.current?.click();
  };

  const handleImport = () => {
    hiddenFileInput.current?.click();
  };

  return (
    <Dialog.Root>
      <Dialog.Trigger asChild>
        <button className="p-2">
          <SettingIcon className="w-5 h-5 dark:text-gray-200" />
        </button>
      </Dialog.Trigger>
      <Dialog.Portal>
        <Dialog.Overlay className="DialogOverlay" />
        <Dialog.Content className="DialogContent">
          <Tabs.Root className="TabsRoot" defaultValue="tab1">
            <Tabs.List className="TabsList flex gap-2 border-0 border-b pb-2">
              <Tabs.Trigger className="TabsTrigger" value="tab1" asChild>
                <button onClick={() => setActiveTab("keyword")}>
                  Keywords
                </button>
              </Tabs.Trigger>
              <Tabs.Trigger className="TabsTrigger" value="tab2" asChild>
                <button onClick={() => setActiveTab("json")}>
                  Export/Import
                </button>
              </Tabs.Trigger>
            </Tabs.List>
            <Tabs.Content className="TabsContent h-[200px]" value="tab1">
              <div className="flex flex-col gap-1 py-3">
                {subreddits_queries.map((item) => (
                  <div key={item} className="px-4 py-2 flex gap-5">
                    <input
                      id={`${item}_checkbox`}
                      type="checkbox"
                      checked={queries.includes(item)}
                      onChange={() => handleCheck(item)}
                    />
                    <label htmlFor={`${item}_checkbox`}>{item}</label>
                  </div>
                ))}
              </div>
            </Tabs.Content>
            <Tabs.Content className="TabsContent h-[200px]" value="tab2">
              <div className="flex flex-col gap-3 py-3">
                <button onClick={handleImport}>Import</button>
                <input
                  ref={hiddenFileInput}
                  type="file"
                  accept=".json"
                  onChange={handleFileUpload}
                  className="hidden"
                />
                <button onClick={handleExport}>Export</button>
              </div>
            </Tabs.Content>
          </Tabs.Root>

          <div className="flex gap-2 justify-end">
            <Dialog.Close asChild>
              <button
                ref={closeRef}
                onClick={handleSave}
                className="bg-purple-500 text-white"
              >
                Save
              </button>
            </Dialog.Close>
            <Dialog.Close asChild>
              <button className="py-2 px-4">Close</button>
            </Dialog.Close>
          </div>
        </Dialog.Content>
      </Dialog.Portal>
    </Dialog.Root>
  );
};
