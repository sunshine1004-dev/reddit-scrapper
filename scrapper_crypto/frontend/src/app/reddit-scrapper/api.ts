import axios from "axios";

const API = import.meta.env.VITE_APP_API || 'http://localhost:5000'
export const getSubreddits = async (queries: string[]) => {
  const res = await axios.get(`${API}/search-subreddit-api/?keywords=${JSON.stringify(queries)}`);
  return res.data.result
};

type CommentParams = {
  search: string;
  subredditName: string;
  page: number;
  timeFilter: string;
};

export const getComments = async ({
  search,
  subredditName,
  page,
  timeFilter,
}: CommentParams) => {
  const res = await axios.get(
    `${API}/search-subreddit-comments-api/?search_keywords=${search}&subreddit_name=${subredditName}&page=${page}&time_filter=${timeFilter}`
  )
  return res.data;
};
