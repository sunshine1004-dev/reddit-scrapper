import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { getComments, getSubreddits } from "./api";
import { SubredditSettingModal } from "./subreddit-setting-modal";
import { DeleteIcon } from "../../components/icons/delete-icon";
import Dark from "../../components/dark";

export default () => {
  const [subReddits, setSubReddits] = useState<string[]>([]);
  const [subRedditsLoading, setSubRedditsLoading] = useState(false);
  const [commentLoading, setCommentLoading] = useState(false);
  const [comments, setComments] = useState<any[]>([]);
  const [timeFilter, setTimeFilter] = useState<string>("all");
  const [keyword, setKeyword] = useState<string>("");
  // const { download } = useDownloader();
  const [searchParams] = useSearchParams();

  useEffect(() => {
    const JSONSubReddits = window.localStorage.getItem("subReddits");
    const redditDetailArr = JSONSubReddits
      ? (JSON.parse(JSONSubReddits) as string[])
      : [];
    setSubReddits(redditDetailArr);
    const detailKeyword = searchParams.get("search_keywords");
    const detailTimeFilter = searchParams.get("time_filter");
    if (detailKeyword) {
      setKeyword(detailKeyword);
      getData(detailKeyword, detailTimeFilter);
    }
    if (detailTimeFilter) {
      setTimeFilter(detailTimeFilter);
    }
  }, []);

  const getData = async (
    detailKeyword: string,
    detailTimeFilter: string | null
  ) => {
    const JSONSubReddits = window.localStorage.getItem("subReddits");
    const redditDetailArr = JSONSubReddits
      ? (JSON.parse(JSONSubReddits) as string[])
      : [];
    let subredditArr: any[] = [];
    setCommentLoading(true);
    for (const subreddit of redditDetailArr) {
      let page = 1;
      while (1) {
        if (page > 20) {
          break;
        }
        try {
          const data = await getComments({
            search: detailKeyword,
            subredditName: subreddit,
            page: page,
            timeFilter: detailTimeFilter ? detailTimeFilter : "all",
          });
          const result = data.result as any[];
          const end = data.end as boolean;
          subredditArr = [...subredditArr, ...result];
          setComments(subredditArr);
          if (end) {
            break;
          }
        } catch (error) {
          console.log(error);
        } finally {
          page++;
        }
      }
    }
    setCommentLoading(false);
  };

  const fetchSbureddits = async (queries: string[]) => {
    setSubRedditsLoading(true);
    const reddits = await getSubreddits(queries);
    setSubRedditsLoading(false);
    let redditDetailArr: string[] = subReddits;
    for (const redditDetail of reddits) {
      if (!subReddits.includes(redditDetail)) {
        redditDetailArr.push(redditDetail);
      }
    }
    setSubReddits(redditDetailArr);
    window.localStorage.setItem("subReddits", JSON.stringify(redditDetailArr));
  };

  const exportJSON = () => {
    const json = JSON.stringify(subReddits);
    const blob = new Blob([json], { type: "application/json" });
    const href = URL.createObjectURL(blob);

    const fileName = "subreddits-data";

    const link = document.createElement("a");
    link.href = href;
    link.setAttribute("download", fileName);

    document.body.appendChild(link);
    link.click();

    link.parentNode?.removeChild(link);
    // Remember to revoke the blob URL after a delay
    setTimeout(() => {
      URL.revokeObjectURL(href);
    }, 100);
  };

  const importJSON = (jsonData: string[]) => {
    localStorage.setItem("subReddits", JSON.stringify(jsonData));
    setSubReddits(jsonData);
  };

  const deleteItem = (selected: string) => {
    const fitlred = subReddits.filter((item) => item !== selected);
    setSubReddits(fitlred);
    localStorage.setItem("subReddits", JSON.stringify(fitlred));
  };

  return (
    <div className="bg-gray-100 dark:bg-gray-800 min-h-screen">
      <div className="container mx-auto py-10">
        <Dark />
        <div className="w-full flex gap-12">
          <div className="w-[250px] flex flex-col gap-1">
            <div className="flex justify-between items-center border-0 border-b pb-2">
              <h4 className="text-2xl font-bold dark:text-white">Subreddits</h4>
              <SubredditSettingModal
                fetchReddits={fetchSbureddits}
                exportJSON={exportJSON}
                importJSON={importJSON}
              />
            </div>
            {subRedditsLoading ? (
              <div className="w-full text-xl dark:text-gray-200">
                Loading...
              </div>
            ) : (
              <div
                className="w-full flex flex-col gap-2"
                id="subreddit-container"
              >
                {subReddits.map((item, key) => (
                  <div className="flex justify-between items-center" key={key}>
                    <a
                      target="_blank"
                      href={`https://www.reddit.com/r/${item}`}
                      className="d-block text-gray-600 dark:text-gray-300"
                    >
                      {item}
                    </a>
                    <button
                      onClick={() => deleteItem(item)}
                      className="p-1 bg-transparent"
                    >
                      <DeleteIcon className="w-5 h-5 dark:text-white text-gray-400 bg-transparent" />
                    </button>
                  </div>
                ))}
              </div>
            )}
          </div>
          <div className="ml-1 mb-8 flex-grow">
            <div className="flex w-full items-center gap-4">
              <label
                htmlFor="search_keywords"
                className="text-xl font-bold dark:text-gray-300"
              >
                Search Keywords:
              </label>
              <input
                type="text"
                id="search_keywords"
                name="search_keywords"
                className="p-1.5"
                value={keyword}
                onChange={(e) => setKeyword(e.target.value)}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    if (keyword) {
                      window.location.href = `/reddit-topics/?search_keywords=${keyword}&time_filter=${timeFilter}`;
                    }
                  }
                }}
              />
              <button
                id="search-btn"
                className="rounded-md px-4 py-2 bg-gray-300"
                onClick={() => {
                  if (keyword) {
                    window.location.href = `/reddit-topics/?search_keywords=${keyword}&time_filter=${timeFilter}`;
                  }
                }}
              >
                Search
              </button>
              <div className="flex gap-4 items-center">
                <label
                  htmlFor="time_filter"
                  className="text-xl font-bold dark:text-gray-300"
                >
                  Time Filter:
                </label>
                <select
                  name="time_filter"
                  id="time_filter"
                  className="rounded-md px-4 py-2 bg-gray-300"
                  value={timeFilter}
                  onChange={(e) => setTimeFilter(e.target.value)}
                >
                  <option value="all">All</option>
                  <option value="day">Day</option>
                  <option value="hour">Hour</option>
                  <option value="week">Week</option>
                  <option value="month">Month</option>
                  <option value="year">Year</option>
                </select>
              </div>
            </div>
            <div className="w-full mt-8 dark:text-gray-300">
              <div className="w-full" id="comment-container">
                {comments.map((item: any, key) => (
                  <div
                    key={key}
                    className="shadow-md w-full reddit-topic-detail px-4 py-1 gap-4 mb-2"
                  >
                    <div className="mb-4">
                      <a
                        href="`${item.url}`"
                        className="text-lg font-bold hover:underline text-gray-600 dark:text-gray-300"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {item.title}
                      </a>
                      <div className="flex items-center text-gray-700 dark:text-gray-300">
                        <span>Posted:</span>
                        <p className="ml-2">{item.posted}</p>
                      </div>
                    </div>
                    {item.comments.map((comment: any, key: number) => (
                      <div
                        className="bg-white shadow-md p-4 mb-4 w-full"
                        key={key}
                      >
                        <p className="text-gray-800">{comment.comment_text}</p>
                        <div className="w-full flex items-center justify-between mt-2">
                          <p className="text-gray-500 text-sm">
                            {comment.posted} by {comment.author}
                          </p>
                          <a
                            href={`https://www.reddit.com${comment.permalink}`}
                            className="text-gray-500 text-sm mt-2 font-bold"
                            target="_blank"
                          >
                            Reply
                          </a>
                        </div>
                      </div>
                    ))}
                  </div>
                ))}
              </div>
              {commentLoading && (
                <div
                  className="w-full text-xl text-center"
                  id="subreddit-comments-loading"
                >
                  Loading...
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
