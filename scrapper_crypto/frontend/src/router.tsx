import { useEffect } from "react";
import { createBrowserRouter, useNavigate } from "react-router-dom";
import RedditScrapper from "./app/reddit-scrapper";
import YoutubeScrapper from "./app/youtube-scrapper";

const Redirect = () => {
  const navigate = useNavigate();
  useEffect(() => navigate("/reddit-topics"), []);

  return null;
};

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Redirect />,
  },
  {
    path: "/reddit-topics",
    element: <RedditScrapper />,
  },
  {
    path: "/youtube-videos",
    element: <YoutubeScrapper />,
  },
]);
