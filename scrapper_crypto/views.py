from dotenv import load_dotenv
import os
import json

from django.shortcuts import render
from django.http import JsonResponse
import json
from googleapiclient.errors import HttpError

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from datetime import datetime, timedelta
from googleapiclient.discovery import build
import praw
from django.core.paginator import Paginator


class ScrapDataAPIReddit(APIView):
    CLIENT_ID_REDDIT = os.environ.get('CLIENT_ID_REDDIT')
    CLIENT_SECRET_REDDIT = os.environ.get('CLIENT_SECRET_REDDIT')
    SELF_AGENT = os.environ.get('USER_AGENT')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.reddit_results = []

    def get(self, request):
        return render(request, 'reddit_results.html')


class ScrapDataAPIYoutube(APIView):
    API_KEY_YOUTUBE = os.environ.get('API_KEY_YOUTUBE')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.youtube_results = []

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def get(self, request, format=None, *args, **kwargs):
        api_key = self.API_KEY_YOUTUBE
        youtube = build('youtube', 'v3', developerKey=api_key)

        search_keywords = request.GET.get('search_keywords', '')

        search_response = []

        search_response = youtube.search().list(
            q=search_keywords,
            part='id,snippet',
            type='video',
            maxResults=20,
            videoSyndicated='true',
        ).execute()

        video_ids = [item['id']['videoId']
                     for item in search_response['items']]
        detailed_comments_response = []

        for video_id in video_ids:
            try:
                comments = youtube.commentThreads().list(
                    part='snippet',
                    videoId=video_id,
                    maxResults=50,
                    searchTerms=search_keywords
                ).execute()

                for comment in comments.get('items', []):
                    if search_keywords.lower() in comment['snippet']['topLevelComment']['snippet']['textDisplay'].lower():
                        detailed_comments_response.append(comment)

            except HttpError as e:
                error_details = json.loads(e.content.decode('utf-8'))
                if any(error.get('reason') == 'commentsDisabled' for error in error_details.get('error', {}).get('errors', [])):
                    print(
                        f"Comments are disabled for video with ID: {video_id}")
                else:
                    print(
                        f"Error fetching comments for video with ID {video_id}: {error_details}")

        return render(request, 'youtube_results.html', {'youtube_results': search_response, 'comments': detailed_comments_response, })


def get_subreddits(request):
    CLIENT_ID_REDDIT = os.environ.get('CLIENT_ID_REDDIT')
    CLIENT_SECRET_REDDIT = os.environ.get('CLIENT_SECRET_REDDIT')
    SELF_AGENT = os.environ.get('USER_AGENT')

    reddit = praw.Reddit(client_id=CLIENT_ID_REDDIT,
                         client_secret=CLIENT_SECRET_REDDIT,
                         user_agent=SELF_AGENT)

    subreddit_queries_string = request.GET.get('keywords', '[]')
    subreddit_queries = json.loads(subreddit_queries_string)

    subreddit_list = []
    for query in subreddit_queries:
        subreddits = reddit.subreddits.search(query=query)
        for subreddit in subreddits:
            subreddit_list.append(subreddit.display_name)

    return JsonResponse({'result': subreddit_list})


def get_subreddit_comments(request):
    CLIENT_ID_REDDIT = os.environ.get('CLIENT_ID_REDDIT')
    CLIENT_SECRET_REDDIT = os.environ.get('CLIENT_SECRET_REDDIT')
    SELF_AGENT = os.environ.get('USER_AGENT')

    reddit = praw.Reddit(client_id=CLIENT_ID_REDDIT,
                         client_secret=CLIENT_SECRET_REDDIT,
                         user_agent=SELF_AGENT)

    query = request.GET.get('search_keywords', None)
    subreddit_name = request.GET.get('subreddit_name', None)
    page = request.GET.get('page', None)
    time_filter = request.GET.get('time_filter', None)

    subreddit = reddit.subreddit(subreddit_name)
    reddit_results = []
    end = False

    subreddit_detail = subreddit.search(
        query, syntax='cloudsearch', sort='new', limit=400, time_filter=time_filter)

    try:
        post_list = [post for post in subreddit_detail]

        if int(page) * 20 < len(post_list) or int(page) == 1:
            paginator = Paginator(post_list, 20)
            page_obj = paginator.get_page(page)
            for submission in page_obj:
                submission.comments.replace_more(limit=None)
                comments = submission.comments.list()

                def modify_comment(comment_datail): return {
                    'posted': datetime.utcfromtimestamp(comment_datail.created_utc).isoformat() if comment_datail.created_utc else '[No date]',
                    'author': comment_datail.author.name if comment_datail.author else '[No author]',
                    'comment_text': comment_datail.body if comment_datail.body else '[No content]',
                    'permalink': comment_datail.permalink if comment_datail.permalink else '[No link]'
                }

                detailed_comment = []

                for comment in list(map(modify_comment, comments)):
                    if query.lower() in comment['comment_text'].lower():
                        detailed_comment.append(comment)

                if len(detailed_comment):
                    reddit_results.append({
                        'title': submission.title,
                        'url': submission.url,
                        # 'iframeURL': submission.url.replace('https://www.reddit.com', 'https://reddit.artemisdigital.io'),
                        'posted': datetime.utcfromtimestamp(submission.created_utc).isoformat(),
                        'id': submission.id,
                        'comments': detailed_comment
                    })
        else:
            reddit_results = []
            end = True
    except praw.exceptions.PRAWException as e:
        # Handle exceptions if needed
        print(f"Error: {e}")

    return JsonResponse({'result': reddit_results, 'end': end})


def get_youtube_comments(request):
    API_KEY_YOUTUBE = os.environ.get('API_KEY_YOUTUBE')
    youtube = build('youtube', 'v3', developerKey=API_KEY_YOUTUBE)

    search_keywords = request.GET.get('search_keywords', '')

    search_response = []

    search_response = youtube.search().list(
        q=search_keywords,
        part='id,snippet',
        type='video',
        maxResults=20,
        videoSyndicated='true',
    ).execute()

    video_ids = [item['id']['videoId']
                 for item in search_response['items']]
    detailed_comments_response = []

    for video_id in video_ids:
        try:
            comments = youtube.commentThreads().list(
                part='snippet',
                videoId=video_id,
                maxResults=50,
                searchTerms=search_keywords
            ).execute()

            for comment in comments.get('items', []):
                if search_keywords.lower() in comment['snippet']['topLevelComment']['snippet']['textDisplay'].lower():
                    detailed_comments_response.append(comment)

        except HttpError as e:
            error_details = json.loads(e.content.decode('utf-8'))
            if any(error.get('reason') == 'commentsDisabled' for error in error_details.get('error', {}).get('errors', [])):
                print(
                    f"Comments are disabled for video with ID: {video_id}")
            else:
                print(
                    f"Error fetching comments for video with ID {video_id}: {error_details}")

    return JsonResponse({'result': detailed_comments_response})
