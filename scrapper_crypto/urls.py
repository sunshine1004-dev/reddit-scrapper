from django.contrib import admin
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView
from .views import ScrapDataAPIReddit, ScrapDataAPIYoutube, get_subreddit_comments, get_subreddits, get_youtube_comments

urlpatterns = [
    path('admin/', admin.site.urls),
    path('search-youtube/', ScrapDataAPIYoutube.as_view(),
         name='search-results-youtube'),
    path('search-reddit/', ScrapDataAPIReddit.as_view(),
         name='search-results-reddit'),
    path('search-subreddit-comments-api/', get_subreddit_comments,
         name='search-subreddit-comments-api'),
    path('search-youtube-comments-api/', get_youtube_comments,
         name='search-youtube-comments-api'),
    path('search-subreddit-api/', get_subreddits, name='search-subreddit-api'),
    path('reddit-topics/', ScrapDataAPIReddit.as_view(), name='reddit-topic-list'),
    path('youtube-videos/', ScrapDataAPIYoutube.as_view(),
         name='youtube-video-list'),
    path('', RedirectView.as_view(url=reverse_lazy('reddit-topic-list'),
         permanent=True), name='scrapper-dashboard')
]
